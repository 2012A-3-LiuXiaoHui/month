package com.bwie.shop.controller;

import com.bwie.common.domain.Manager;
import com.bwie.common.domain.Shop;
import com.bwie.common.result.Result;
import com.bwie.shop.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.swing.plaf.multi.MultiFileChooserUI;
import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 10:25
 * @Version v1.0
 */
@RestController
public class ShopController {
    @Autowired
    ShopService shopService;
    @PostMapping("list")
    public Result<List<Shop>> list(){
        return shopService.list();
    }
    @PostMapping("fastdfs")
    public Result fastdfs(@RequestParam("file")MultipartFile multipartFile){
        return shopService.fastdfs(multipartFile);
    }
    @PostMapping("InsertShop")
    public Result InsertShop(@RequestBody Shop shop){
        return shopService.InsertShop(shop);
    }
    @PostMapping("ManagerList/{shopId}")
    public Result<List<Manager>> ManagerList(@PathVariable Integer shopId){
        return shopService.ManagerList(shopId);
    }
    @PostMapping("InsertManager/{shopId}/{type}")
    public Result InsertManager(@PathVariable Integer shopId,@PathVariable String type){
        return shopService.InsertManager(shopId,type);
    }
    @PostMapping("updateStatus/{managerId}/{status}/{userId}")
    public Result updateStatus(@PathVariable Integer managerId,@PathVariable Integer status,@PathVariable Integer userId){
        return shopService.updateStatus(managerId,status,userId);
    }
}
