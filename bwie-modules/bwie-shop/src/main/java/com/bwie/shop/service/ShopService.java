package com.bwie.shop.service;

import com.bwie.common.domain.Manager;
import com.bwie.common.domain.Shop;
import com.bwie.common.result.Result;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 10:26
 * @Version v1.0
 */
public interface ShopService {
    Result<List<Shop>> list();

    Result fastdfs(MultipartFile multipartFile);

    Result InsertShop(Shop shop);

    Result<List<Manager>> ManagerList(Integer shopId);

    Result InsertManager(Integer shopId, String type);

    Result updateStatus(Integer managerId, Integer status,Integer userId);
}
