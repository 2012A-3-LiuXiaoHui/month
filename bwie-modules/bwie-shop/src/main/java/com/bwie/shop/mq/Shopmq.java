package com.bwie.shop.mq;

import com.alibaba.fastjson.JSONObject;
import com.bwie.common.constants.MqConstants;
import com.bwie.common.domain.Shop;
import com.bwie.common.utils.MsgUtil;
import com.bwie.shop.service.ShopService;
import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 14:21
 * @Version v1.0
 */
@Component
@Log4j2
public class Shopmq {
    @Autowired
    RedisTemplate<String,String> redisTemplate;
    @Autowired
    ShopService service;

    @RabbitListener(queuesToDeclare = { @Queue(name = MqConstants.CHECK_NAME)})
    public void send(String msg, Message message, Channel channel){
        log.info("mq开始消费");
        String messageId = message.getMessageProperties().getMessageId();
        Long add = redisTemplate.opsForSet().add(MqConstants.CHECK_NAME, messageId);
        if(add==1){
            MsgUtil.sendMsg(msg,"ok");
            try {
                channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            } catch (IOException e) {
                redisTemplate.opsForSet().remove(MqConstants.CHECK_NAME,messageId);

                try {
                    channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
                throw new RuntimeException(e);
            }
        }
    }
}
