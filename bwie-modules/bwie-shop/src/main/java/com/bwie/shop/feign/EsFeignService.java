package com.bwie.shop.feign;

import com.bwie.common.domain.Shop;
import com.bwie.common.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 14:44
 * @Version v1.0
 */
//@FeignClient(name = "bwie-es")
public interface EsFeignService {
    @PostMapping("InsertShop")
    public Result InsertSHop(@RequestBody Shop shop);
}
