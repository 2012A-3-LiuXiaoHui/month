package com.bwie.shop.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bwie.common.constants.MqConstants;
import com.bwie.common.constants.TokenConstants;
import com.bwie.common.domain.Manager;
import com.bwie.common.domain.Shop;
import com.bwie.common.domain.User;
import com.bwie.common.result.Result;
import com.bwie.common.utils.JwtUtils;
import com.bwie.shop.FastUtil;
import com.bwie.shop.mapper.ShopMapper;
import com.bwie.shop.service.ShopService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 10:26
 * @Version v1.0
 */
@Service
public class ShopServiceImpl implements ShopService {
    @Autowired
    ShopMapper shopMapper;
    @Autowired
    FastUtil fastUtil;
    @Autowired
    HttpServletRequest request;
    @Autowired
    RedisTemplate<String,String> redisTemplate;
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Override
//    c.实现店铺信息的展示，主要显示内容参见上图（3分）
    public Result<List<Shop>> list() {
        List<Shop> list = shopMapper.list();
        return Result.success(list);
    }

    @Override
    public Result fastdfs(MultipartFile multipartFile) {
        try {
            String upload = fastUtil.upload(multipartFile);
            if(upload!=null){
                return Result.success("http://124.222.108.225:8888/"+upload,"上传成功");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return Result.error("上传失败");
    }

    @Override
    public Result InsertShop(Shop shop) {
        User info = this.info();
        shop.setUserId(info.getUserId());
//        e.添加店铺需要缴纳500元保证金（3分）
        shopMapper.updateMoney(info.getUserId());
        shopMapper.InsertShop(shop);
//        b.添加、修改、禁用店铺信息，使用MQ进行异步更新ES索引库（3分）
        rabbitTemplate.convertAndSend(MqConstants.MQ_NAME,JSONObject.toJSONString(shop),message -> {
            message.getMessageProperties().setMessageId(UUID.randomUUID().toString());
            return message;
        });
        return Result.success();
    }

    @Override
    public Result<List<Manager>> ManagerList(Integer shopId) {
        List<Manager> managers = shopMapper.ManagerList(shopId);
        return Result.success(managers);
    }

    @Override
    public Result InsertManager(Integer shopId, String type) {
        shopMapper.InsertManager(shopId,type);
        return Result.success();
    }

    @Override
    public Result updateStatus(Integer managerId, Integer status,Integer userId) {
        User byPhone = shopMapper.findByPhone(userId);
//        e)审核通过与审核不通过，需要异步给相关负责人发送短信（2分）
        rabbitTemplate.convertAndSend(MqConstants.CHECK_NAME,byPhone.getPhone(),message -> {
            message.getMessageProperties().setMessageId(UUID.randomUUID().toString());
            return message;
        });
        shopMapper.updateStatus(managerId,status);
        return Result.success();
    }

    public User info() {
        String token = request.getHeader(TokenConstants.TOKEN);
        String userKey = JwtUtils.getUserKey(token);
        String s = redisTemplate.opsForValue().get(TokenConstants.LOGIN_TOKEN_KEY + userKey);
        return JSONObject.parseObject(s,User.class);
    }
}
