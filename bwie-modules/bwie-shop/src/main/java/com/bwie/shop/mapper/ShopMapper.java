package com.bwie.shop.mapper;

import com.bwie.common.domain.Manager;
import com.bwie.common.domain.Shop;
import com.bwie.common.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 10:27
 * @Version v1.0
 */
@Mapper
public interface ShopMapper {
    List<Shop> list();

    void InsertShop(Shop shop);

    void updateMoney(Integer userId);

    List<Manager> ManagerList(Integer shopId);

    void InsertManager(@Param("shopId") Integer shopId, @Param("type") String type);

    void updateStatus(@Param("managerId") Integer managerId, @Param("status") Integer status);

    User findByPhone(Integer userId);
}
