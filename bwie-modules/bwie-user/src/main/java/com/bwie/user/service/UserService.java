package com.bwie.user.service;

import com.bwie.common.domain.User;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:21
 * @Version v1.0
 */
public interface UserService {
    User findByPhone(String phone);
}
