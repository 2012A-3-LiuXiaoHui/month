package com.bwie.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description ${description}
 * @Author 26704
 * @LastChangeDate 2023/8/9 8:57
 * @Version v1.0
 */
@SpringBootApplication
public class UserApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class,args);
    }
}