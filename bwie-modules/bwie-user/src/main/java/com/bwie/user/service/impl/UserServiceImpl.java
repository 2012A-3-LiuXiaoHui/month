package com.bwie.user.service.impl;

import com.bwie.common.domain.User;
import com.bwie.user.mapper.UserMapper;
import com.bwie.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:22
 * @Version v1.0
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;
    @Override
    public User findByPhone(String phone) {
        return userMapper.findByPhone(phone);
    }
}
