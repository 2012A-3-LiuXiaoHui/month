package com.bwie.user.controller;

import com.bwie.common.domain.User;
import com.bwie.common.result.Result;
import com.bwie.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:20
 * @Version v1.0
 */
@RestController
public class UserController {
    @Autowired
    UserService userService;
    @PostMapping("findByPhone/{phone}")
    public Result<User> findByPhone(@PathVariable String phone){
        User byPhone = userService.findByPhone(phone);
        return Result.success(byPhone);
    }
}
