package com.bwie.user.mapper;

import com.bwie.common.domain.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:23
 * @Version v1.0
 */
@Mapper
public interface UserMapper {
    User findByPhone(String phone);
}
