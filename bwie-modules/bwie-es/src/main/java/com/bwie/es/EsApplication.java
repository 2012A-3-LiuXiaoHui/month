package com.bwie.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Description ${description}
 * @Author 26704
 * @LastChangeDate 2023/8/9 11:45
 * @Version v1.0
 */
@SpringBootApplication
@EnableFeignClients
public class EsApplication {
    public static void main(String[] args) {
        SpringApplication.run(EsApplication.class,args);
    }
}