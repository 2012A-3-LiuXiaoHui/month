package com.bwie.es.service;

import com.bwie.common.domain.Shop;
import com.bwie.common.domain.request.QueryShop;
import com.bwie.common.result.Result;

import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 12:17
 * @Version v1.0
 */

public interface EService {
    Result<List<Shop>> list(QueryShop queryShop);

    Result InsertShop(Shop shop);
}
