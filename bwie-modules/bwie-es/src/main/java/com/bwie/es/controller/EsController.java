package com.bwie.es.controller;

import com.bwie.common.domain.Shop;
import com.bwie.common.domain.request.QueryShop;
import com.bwie.common.result.Result;
import com.bwie.es.service.EService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 12:14
 * @Version v1.0
 */
@RestController
public class EsController {
    @Autowired
    EService eService;
    @PostMapping("list")
    public Result<List<Shop>> list(@RequestBody QueryShop queryShop){
        return eService.list(queryShop);
    }
    @PostMapping("InsertShop")
    public Result InsertShop(@RequestBody Shop shop){
        return eService.InsertShop(shop);
    }
}
