package com.bwie.es.config;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.C;
import lombok.Data;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 11:58
 * @Version v1.0
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "es")
public class InitEsHigLevelChilent {
    private String host;
    private Integer port;
    private String scheme;
    @Bean
    public RestHighLevelClient restHighLevelClient(){
        return new RestHighLevelClient(
                RestClient.builder(new HttpHost(host,port,scheme))
        );
    }
}
