package com.bwie.es.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import com.bwie.common.constants.EsConstants;
import com.bwie.common.domain.Shop;
import com.bwie.common.domain.request.QueryShop;
import com.bwie.common.result.Result;
import com.bwie.common.utils.StringUtils;
import com.bwie.es.service.EService;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 12:17
 * @Version v1.0
 */
@Service
public class EServiceImpl implements EService {
    @Autowired
    RestHighLevelClient restHighLevelClient;
    @Override
    public Result<List<Shop>> list(QueryShop queryShop) {
        ArrayList<Shop> list = new ArrayList<>();
        SearchRequest searchRequest = new SearchRequest(EsConstants.INDEX_NAME);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
//        d.根据店铺名称使用IK分词查询，店铺录入时间区间查询，店铺名称进行高亮（3分）
        if(!StringUtils.isEmpty(queryShop.getName())) {
            boolQueryBuilder.must(QueryBuilders.matchQuery("name", queryShop.getName()));
        }
        if(queryShop.getStartDate()!=null){
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createDate").gt(queryShop.getStartDate().getTime()));
        }
        if(queryShop.getEndDate()!=null){
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createDate").lt(queryShop.getEndDate().getTime()));
        }
        searchSourceBuilder.query(boolQueryBuilder);
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("name").preTags("<font style=\"color:red;\">").postTags("</font>");
        searchSourceBuilder.highlighter(highlightBuilder);
        searchRequest.source(searchSourceBuilder);
        try {
            SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = search.getHits();
            SearchHit[] hits1 = hits.getHits();
            for (SearchHit documentFields : hits1) {
                String sourceAsString = documentFields.getSourceAsString();
                Shop shop = JSONObject.parseObject(sourceAsString, Shop.class);
                Map<String, HighlightField> highlightFields = documentFields.getHighlightFields();
                if(highlightFields!=null){
                    HighlightField name = highlightFields.get("name");
                    if(name!=null){
                        Text[] fragments = name.getFragments();
                        String str="";
                        for (Text fragment : fragments) {
                            str+=fragment;
                        }
                        shop.setName(str);
                    }
                }
                list.add(shop);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return Result.success(list);
    }

    @Override
    public Result InsertShop(Shop shop) {
        IndexRequest indexRequest = new IndexRequest(EsConstants.INDEX_NAME);
        indexRequest.source(JSONObject.toJSONString(shop), XContentType.JSON);
        try {
            restHighLevelClient.index(indexRequest,RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return Result.success();
    }
}
