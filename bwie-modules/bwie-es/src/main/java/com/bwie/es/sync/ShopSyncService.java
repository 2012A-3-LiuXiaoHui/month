package com.bwie.es.sync;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import com.bwie.common.constants.Constants;
import com.bwie.common.constants.EsConstants;
import com.bwie.common.domain.Shop;
import com.bwie.common.result.Result;
import com.bwie.es.feign.ShopFeignService;
import io.jsonwebtoken.lang.Collections;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description es同步
 * @Author 26704
 * @LastChangeDate 2023/8/9 11:47
 * @Version v1.0
 */
@Component
public class ShopSyncService implements ApplicationRunner {
    @Autowired
    ShopFeignService shopFeignService;
    @Autowired
    RestHighLevelClient restHighLevelClient;
    @Override
//    a.项目启动，初始化店铺信息到ES索引库当中（3分）
    public void run(ApplicationArguments args) throws Exception {
        Result<List<Shop>> list = shopFeignService.list();
        if(list.getCode()== Constants.ERROR){
            throw new RuntimeException(list.getMsg());
        }
        List<Shop> data = list.getData();
        if (!Collections.isEmpty(data)) {
            BulkRequest bulkRequest = new BulkRequest();
            for (Shop datum : data) {
                SimplePropertyPreFilter filter = new SimplePropertyPreFilter();
                filter.getExcludes().add("id");
                IndexRequest indexRequest = new IndexRequest(EsConstants.INDEX_NAME);
                indexRequest.id(datum.getShopId()+"");
                indexRequest.source(JSONObject.toJSONString(datum), XContentType.JSON);
                bulkRequest.add(indexRequest);
                restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
            }
        }
    }
}
