package com.bwie.es.feign;

import com.bwie.common.domain.Shop;
import com.bwie.common.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 11:48
 * @Version v1.0
 */
@FeignClient(name = "bwie-shop")
public interface ShopFeignService {
    @PostMapping("list")
    public Result<List<Shop>> list();
}
