package com.bwie.common.domain;

import lombok.Data;

import java.util.Date;

/**
 * @Description 店铺
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:16
 * @Version v1.0
 */
@Data
public class Shop {
    private Integer shopId;
    private String name;
    private Integer userId;
    private String userName;
    private Date createDate;
    private Date startDate;
    private Date endDate;
    private String pic;
    private Integer managerId;
    private Integer status;
    private String other;
    private Integer num;
}
