package com.bwie.common.domain.response;

import lombok.Data;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:18
 * @Version v1.0
 */
@Data
public class JwtResponse {
    private String token;
    private String exprieTime;
}
