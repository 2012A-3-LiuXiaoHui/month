package com.bwie.common.domain.request;

import lombok.Data;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:19
 * @Version v1.0
 */
@Data
public class LoginUser {
    private String phone;
    private String code;
}
