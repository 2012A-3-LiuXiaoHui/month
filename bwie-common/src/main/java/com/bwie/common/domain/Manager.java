package com.bwie.common.domain;

import lombok.Data;

import java.util.Date;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 11:12
 * @Version v1.0
 */
@Data
public class Manager {
    private Integer managerId;
    private Integer shopId;
    private String name;
    private String type;
    private Integer userId;
    private String userName;
    private Date createDate;
    private Date startDate;
    private Date endDate;
    private String pic;
    private String other;
    private Integer num;
    private Integer status;
}
