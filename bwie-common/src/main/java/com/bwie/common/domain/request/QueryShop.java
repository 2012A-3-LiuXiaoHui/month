package com.bwie.common.domain.request;

import lombok.Data;

import java.util.Date;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 12:15
 * @Version v1.0
 */
@Data
public class QueryShop {
    private String name;
    private Date startDate;
    private Date endDate;
}
