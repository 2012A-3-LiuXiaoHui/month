package com.bwie.common.domain;

import lombok.Data;

/**
 * @Description 用户
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:15
 * @Version v1.0
 */
@Data
public class User {
    private Integer userId;
    private String userName;
    private String phone;
    private Integer role;
    private Integer money;
}
