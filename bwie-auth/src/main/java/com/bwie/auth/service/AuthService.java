package com.bwie.auth.service;

import com.bwie.common.domain.User;
import com.bwie.common.domain.request.LoginUser;
import com.bwie.common.domain.response.JwtResponse;
import com.bwie.common.result.Result;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:32
 * @Version v1.0
 */
public interface AuthService {
    Result<JwtResponse> login(LoginUser loginUser);

    Result getCode(String phone);

    User info();
}
