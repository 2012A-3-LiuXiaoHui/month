package com.bwie.auth.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.bwie.auth.service.AuthService;
import com.bwie.common.constants.JwtConstants;
import com.bwie.common.constants.TokenConstants;
import com.bwie.common.domain.User;
import com.bwie.common.domain.request.LoginUser;
import com.bwie.common.domain.response.JwtResponse;
import com.bwie.common.result.Result;
import com.bwie.common.utils.JwtUtils;
import com.bwie.common.utils.MsgUtil;
import com.bwie.common.utils.StringUtils;
import com.bwie.auth.feign.AuthFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:32
 * @Version v1.0
 */
@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    AuthFeignService authFeignService;
    @Autowired
    HttpServletRequest request;
    @Autowired
    RedisTemplate<String,String> redisTemplate;
    @Override
    public Result<JwtResponse> login(LoginUser loginUser) {
//        a.实现短信验证码登录功能（1分）
        Result<User> byPhone = authFeignService.findByPhone((loginUser.getPhone()));
        User user = byPhone.getData();
        if (StringUtils.isAnyEmpty(loginUser.getPhone(), loginUser.getCode())) {
            return Result.error("手机号验证码不能为空");
        }
        String s = redisTemplate.opsForValue().get(loginUser.getPhone());
        if(!loginUser.getCode().equals(s)){
            return Result.error("验证码错误");
        }
        HashMap<String, Object> map = new HashMap<>();
        String userKey = UUID.randomUUID().toString().replaceAll("-", "");
        map.put(JwtConstants.USER_KEY,userKey);
        String token = JwtUtils.createToken(map);
        redisTemplate.opsForValue().set(TokenConstants.LOGIN_TOKEN_KEY+userKey, JSONObject.toJSONString(user),TokenConstants.EXPIRATION, TimeUnit.MINUTES);
        JwtResponse jwtResponse = new JwtResponse();
//        d.使用JWT 实现系统鉴权功能，未登录用户只能访问登录和发送验证码功能，其他功能不能访问（3分）
        jwtResponse.setToken(token);
//        e.用户信息保存到redis当中，JWT过期时间为20分钟（2分）
        jwtResponse.setExprieTime("20MIN");
        return Result.success(jwtResponse);
    }

    @Override
    public Result getCode(String phone) {
        Result<User> byPhone = authFeignService.findByPhone(phone);
        User user = byPhone.getData();
        if(user!=null){
            String code="";
//            b.验证码随机生成（1分）
            for (int i = 0; i < 4; i++) {
                code+=new Random().nextInt(10);
            }
            MsgUtil.sendMsg(phone,code);
//            c.验证码以手机号保存到redis当中（3分）
            redisTemplate.opsForValue().set(phone,code);
            return Result.success(code);
        }
        return Result.error();
    }

    @Override
    public User info() {
        String token = request.getHeader(TokenConstants.TOKEN);
        String userKey = JwtUtils.getUserKey(token);
        String s = redisTemplate.opsForValue().get(TokenConstants.LOGIN_TOKEN_KEY + userKey);
        return JSONObject.parseObject(s,User.class);
    }
}
