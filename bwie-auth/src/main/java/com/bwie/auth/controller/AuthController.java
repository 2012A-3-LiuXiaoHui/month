package com.bwie.auth.controller;

import com.bwie.auth.service.AuthService;
import com.bwie.common.domain.User;
import com.bwie.common.domain.request.LoginUser;
import com.bwie.common.domain.response.JwtResponse;
import com.bwie.common.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:31
 * @Version v1.0
 */
@RestController
public class AuthController {
    @Autowired
    AuthService authService;
    @PostMapping("getCode/{phone}")
    public Result getCode(@PathVariable String phone){
        return authService.getCode(phone);
    }
    @PostMapping("login")
    public Result<JwtResponse> login(@RequestBody LoginUser loginUser){
        return authService.login(loginUser);
    }
    @PostMapping("info")
    public Result<User> info(){
        User info = authService.info();
        return Result.success(info);
    }
}
