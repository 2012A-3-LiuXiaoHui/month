package com.bwie.auth.feign;

import com.bwie.common.domain.User;
import com.bwie.common.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/9 9:34
 * @Version v1.0
 */
@FeignClient(name = "bwie-user")
public interface AuthFeignService {
//    b.采用OpenFeign进行服务之间调用（2分）
    @PostMapping("findByPhone/{phone}")
    public Result<User> findByPhone(@PathVariable String phone);
}
